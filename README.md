# 2018-10-01

* Tour de table: présentation, pourquoi avoir choisi ce module
* Présentation du module
* Monstration de différents objets graphiques soit hybides soit créés avec des logiciels libres

Liste des objets présentés:
* Wim Nijenhuis, *The Riddle of the Real City or the Dark Knowledge of Urbanism*,1001 Publishers & INC / livre / design graphique: Open Source Publishing [(lien)](http://networkcultures.org/blog/publication/the-riddle-of-the-real-city-or-the-dark-knowledge-of-urbanism/)
* Catherine Lenoble & OSP, *Anna K*, éditions HYX / livre / design graphique: Open Source Publishing [(lien)](http://www.editions-hyx.com/fr/anna-k)
* *Code X: 01 - PrePostPrint*, éditions HYX / journal / design graphique: Julie Blanc et Quentin Juhel [(lien)](http://www.editions-hyx.com/fr/code-x)
* *From Bits to Paper*, Filipe Pais (dir.), Paris, Art Book Magazine, 2018, 196 pages. / livre imprimé (24x36cm) + livre numérique (ePub fixed layout enrichi) / conception multisupport : Lucile Haute ; design graphique : Jean François Boulan ; design interactif : Olivain Porry ; intégration ePub : Sophie Ciriello. [(lien)](http://www.ensadlab.fr/fr/francais-lancement-de-louvrage-from-bits-to-paper/) 
* *Speaking Volumes (art activisme et éditions féministes), version 0.4* / fanzine / design graphique: Loraine Furter. [(lien)](http://lorainefurter.net/projects/speaking-volumes/)
* *Poisson Évêque* / livre, fanzine / design graphique: Luuse. [(lien)](https://gitlab.com/Luuse/poisson-eveque)
* *Vogue Homme Japan Digital Vol.1*, Conde Nast, 2010. / magazine de mode enrichi (portfolio animé, son, vidéo) et interactif (menu hypertexte, animations, appel de légendes), application pour iPad, retiré de l'applestore, non compatible iOS 11. 
* divers brochures et programmes fait pour/par PrePostPrint et Open source Publishing
* foulard fait avec TexTuring [(lien)](http://ivan-murit.fr/works.php?w=texturing&p=download.htm) et/ou Stochaster [(lien)](http://www.stochaster.org/)
* travaux de Bonjour Monde: Gutenbug + robot typo [(lien)](http://bonjourmonde.net/)
* *Imprimer le monde*, Marie-Ange Brayer (dir.), Éditions HYX / Éditions du Centre Pompidou, 2017. [(lien)](http://www.editions-hyx.com/fr/imprimer-le-monde)
* *Night of the Living Dead Pixel*, editions Volumiques, 2011. / livre augmenté support d'un jeu vidéo pour iPhone, mis à jour iOS11.
[(lien1)](https://volumique.com/v2/portfolio/night-of-the-living-dead-pixels/), [(lien2)](https://itunes.apple.com/app/keynote/id589840995?mt=8)
* *Dear lulu*, James Goggin (dir.), 14,81 x 20,98 cm, 96p. 2008. / print on demand book. [(lien)](http://p-dpa.net/work/dear-lulu/)
* Art Book Magazine, application pour iPad, librairie-bibliothèque dédiée à la création contemporaine (art, art contemporain, design, architecture, photographie). [(lien)](https://www.artbookmagazine.com/fr)
* LIS-A / application pour iPad et iPone, application de lecture universelle de publications numériques [(lien)](https://itunes.apple.com/fr/app/lis-a/id1096168122?mt=8)





# 2018-10-08

* Pourquoi militer pour le libre
* Explication des chaînes éditoriales
* Explication du *web-to-print* + démo
* Initiation à HTML et CSS: [fichiers d'exemples](https://gitlab.com/JulieBlanc/ensad-mrc-2018/tree/master/html_2018-10-08)

Slides de Julie présentées pendant le cours : [http://recherche.julie-blanc.fr/revealjs/20180820_Lure.html](http://recherche.julie-blanc.fr/revealjs/20180820_Lure.html)

