# Premiers pas en HTML et CSS

## Structuration du dossier

Un projet correspond à un dossier dans lequel vous aller mettre tous vos fichiers (html, css, images, fonts...). Il est important de ne pas déplacer les fichiers à l'intérieur de ce dossier car les éléments seront appeler selon leur emplacement d'un fichier à l'autre. C'est ce que l'on appelle des chemins.  

Les noms des fichiers ne doivent pas contenir d'espace ou d'accents.

Le fichier le plus important est le fichier html, c'est depuis lui que sont appelés tous les autres fichiers et éléments.

## Structuration du fichier HTML

Un fichier HTML minimum contient une balise `<html>`qui contient elle-même deux autres balises `<head>` et `<body>`.
```
<!DOCTYPE html>
<html>

    <head>
    </head>

    <body>
    </body>

</html>
```

Dans le `<head>` on appelle les feuilles de styles, les fonts ou les scripts, on met aussi les métadonnées de la page. Globalement, ce sont tous les éléments qui ne sont pas le contenu.

Dans le `<body>`, c'est le contenu de la page, ce qui va s'afficher dans le navigateur.


## Éléments html
Structuration sémantique d'un contenu

Commentaire  
`<!-- Mom commentaire -->`


### Balises de type bloc

créer un paragraphe  
`<p></p>`


Titre (6 nivreaux)  
`<h1>Mon titre</h1>`
`<h2>...<h2>`
`<h3>...<h3>`
`<h4>...<h4>`
`<h5>...<h5>`
`<h6>...<h6>`

Définir un id (il y a qu'un seul élément "intro" dans le document)  
`<p id="intro"></p>`

Définir une class  
`<p class="citation"></p>`


Ajouter une image  
`<img src="lien-vers-mon-image/nom-de-mon-image.jpg">`

Créer une liste
```
<ul>
    <li>Pomme</li>
    <li>Poire</li>
    <li>Abricot</li>
</ul>
```

Une balise peut contenir d'autre balise.  
Il est d'usage de mettre des indentations (des retraits vers la droite) avant les balises contenus pour une meilleure lecture du code.  
Dans l'exemple suivant, on créé un chapitre qui contient des titres et des paragraphes:
```
<section class="chapitre" id="chapitre_1">
    <h1>Titre de mon chapitre 1</h1>
    <p>Premier paragraphe du chapitre</p>
    <p>Deuxièmes paragraphe du chapitre</p>
</section>
```

On dit dans ce cas que l'élément `<h1>` est un **élément enfant** de l'élément `<section>` ou que l'élément `<section>` est un élément **parent** de l'élément `h1`




### Balises de types Inline
Elles sont contenues dans les balises de type block

Lien  
`<a href="http://mon-site.fr">texte</a>`

Italique  
`<p>Il y a un mot en <em>italique</em> dans ce paragraphe.`

Mise en avant (gras)  
`<p>Il y a un bout de phrase <strong>mise en avant<strong> dans ce paragraphe.`


## css
Mise en forme du contenu

### Sélecteurs CSS

Sélectionner un élément 
`h1{ color: red; }`

Sélectionner un id  
`#intro{ color: red; }`

Sélectionner une class  
`.citation{ color: red; }`

Sélection un élément (paragraphe...) dans un autre (...du chapitre 1)  
`#chapitre_1 p{ color: red; }`


### Quelques propriétés CSS

#### Couleurs
Les couleurs sont définies par:
- des mots: `red`, `blue`, `green`, `black`
- un code hexadécimal: `#ef8790`, `#222222`
- un code rgb (red, green, blue): `rgb(150, 140, 67)`

Couleur du texte  
`h1{ color: red; }`

Couleur de l'arrière plan  
`h1{ background-color: red; }`


#### Fonts

**Famille de caractère**   
Certaines fonts sont par défaut dans tous les navigateurs mais il y en a pas beaucoup (trouver la liste)  
`h1{ font-family: 'arial, sans-serif'; }`  

Si vous voulez ajouter votre propre font, il faut créer un lien vers la font. Nous verrons cela un autre jour.

**Taille de la font**  
`h1{ font-size: 12pt; }`  
(attention, il existe plusieurs unités possible, voir ci-dessous)

**Interlignage**  
`h1{ line-height: 14pt; }`

**Italique**  
`h1{ font-style: italique; }`  
pour enlever l'italique: `h1{ font-style: normal; }`

**Graisse**  
`h1{ font-weight: bold; }`  

Si une famille a plusieurs graisses, différentes graisses des fonts (thin, bold, extra-bolf...) sont définies par des nombres multiples de 100.  
`h1{ font-weight: 300; }`  

#### Margins, paddings, border

**Margin**  
Les `margin` sont les marges extérieures de l'élément, soir la distance entre les éléments.  
On peut les appliquer sur tous les côtés:   
`h1{ margin: 30px }` 

Ou pour un ou plusieurs côté:
```
h1{ 
    margin-left: 30px;
    margin-top: 20px;
}
```

**Padding**  
Les `padding` sont les marges intérieurs de l'élément.  
`h1{ padding: 30px }`   
Si vous appliquer un `background-color` et que vous voulez que votre contenu soit plus éloigné de la bordure de l'élément, c'est les `padding` que vous utiliser.

**Bordures**  
Les bordures sont contituées de trois élements: l'épaisseur de la bordure, son style (`solid`, `double`, pointillés: `dashed`) et sa couleur. On sépare ces trois éléments par un espace.  
`h1{ border: 2px solid #222222; }`

#### Hauteur et largeur des éléments

largeur = `width`  
hauteur = `height`

`img{ width: 200px; }`

Il est assez courant de définir des largeurs de bloc en pourcentage pour qu'ils s'adaptent à la largeur de la fenêtre (voir la partie "unités CSS ci-dessous):  
`img{ width: 90%; }`




#### Unités CSS

##### Unités abolues
Nous vous conseillons d'utiliser le pixel plutôt que le point car les navigateurs arrondissent le poin

point: `14pt`  
pixel: `14px`  
millimètres (uniquement si vous faites de l'imprimé): `20mm`

##### Unitès relatives 
Les unités relatives sont des multiples des unités absolues. Elles permettent de garder les proportions si vous souhaitez agrandir tout votre document (utile pour le responsive).

**rem**  
`rem` est un multiple du font-size définie pour tout le document.  
Par défaut, il est généralement de 12px (mais cela dépend des navigateurs).   
Si vous souhaitez le changer: `body{ font-size: 16px; }`  
Utilisation du rem: `h1{ font-size: 2rem; }`   
Dans cette exemple, le titre aura une hauteur de 32px (16px x 2)

**em**  
`em` est un multiple par rapport à l'élément parent.   
Par exemple, vous définissez des `font-size` différents en fonction des sections:  

```
#chapitre_1{ font-size: 14px; }
#chapitre_2{ font-size: 20px; }
h1{ font-sier: 2em; }
```

Dans cet exemple, les titres `h1` contenu dans la section #chapitre_1 auront une taille de 24px (14px x 2) et les titres `h1` contenu dans la section #chapitre_2 auront une taille de 40px (20px x 2)

**pourcentage**  
On utilise le pourcentage essentiellement pour la taille des éléments. Le pourcentage est toujours défini par rapport à l'élément parent. (S'il en a pas, ce sera par rapport à la fenêtre)
```
section{ width: 80% }
section p{ width: 100%; }
```
Dans cet exemple, une section fait 80% de la largeur de la fenêtre et un paragraphe (contenu dans une section) fait 100% de la taille de la section.

## Quelques liens utiles

* Documentation et tutoriels HTML:
[https://developer.mozilla.org/fr/docs/Web/HTML](https://developer.mozilla.org/fr/docs/Web/HTML)
* Documentation et tutoriels CSS:
[https://developer.mozilla.org/fr/docs/Web/CSS](https://developer.mozilla.org/fr/docs/Web/CSS)
* Mémento des balises HTML:[https://openclassrooms.com/fr/courses/1603881-apprenez-a-creer-votre-site-web-avec-html5-et-css3/1608357-memento-des-balises-html](https://openclassrooms.com/fr/courses/1603881-apprenez-a-creer-votre-site-web-avec-html5-et-css3/1608357-memento-des-balises-html)
* Mémento des balises CSS: [https://openclassrooms.com/fr/courses/1603881-apprenez-a-creer-votre-site-web-avec-html5-et-css3/1608902-memento-des-proprietes-css](https://openclassrooms.com/fr/courses/1603881-apprenez-a-creer-votre-site-web-avec-html5-et-css3/1608902-memento-des-proprietes-css)

